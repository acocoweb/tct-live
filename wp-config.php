<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tct-online-copy');


/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WyQW*;KDX9Yx<.wXPOF6Ny|LB%%796:/YNI}jQj2!g*?BO_f}ZV?XTBT#!&iR%p-');
define('SECURE_AUTH_KEY',  '>:+I)4jo(A{P]a>H.t3xlZR.(o<3AuZ78aB33px[P.DUY5n@l&M+N`SsNS5}l@Cq');
define('LOGGED_IN_KEY',    ';}Om;3:>6gWMKBDkOxYK&S:4*((r}28S,6g$,TVz }(4t$NpQGqEKWZ8/K{*J&JD');
define('NONCE_KEY',        'BZ^P^XvyvnaN,Fb*VfsHx~R./RGg0[IY_IUa~&,=`+ZB2Z#7K9{V&%G`(%(Y,4Jo');
define('AUTH_SALT',        'prIN+J#,Wl#6IAE&[t/Mf4]Xs&4N^k`O?uCC/RhPZ_,eN{-+uVl.wIZ&~$Yv ZX5');
define('SECURE_AUTH_SALT', 'v*ScG;LNK^/>E,1,7~J#iIHc-lPa:8^K/X+j?+o(Toh/haW2;Ee`/X1wcL[3zLuo');
define('LOGGED_IN_SALT',   'Q~f17#}>9!%w<52u O~ha}vM%F;K.UZO{4|EFm#c&{FfB{5|4{A&W4T-|m*vnAv7');
define('NONCE_SALT',       '@Mm}q[dCh]WX=l1S>vzun5IyGqg&|YrsIkM(.N+85)28jS$6z1laOpj;nIyR!h#~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



/** Disable "virtual" cron jobs from WP, because there is a cronjob already running on the server */
/* wget -q -O - https://thecooktown.com.au/tct/wp-cron.php?doing_wp_cron --> Every hour*/ 
define('DISABLE_WP_CRON', true); 

/* Hide warnings */
define('WP_DEBUG_DISPLAY', false);