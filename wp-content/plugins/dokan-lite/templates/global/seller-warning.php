<?php
/**
 *  Dokan Seller Enabled/Disabled warning Notice
 *
 *  @since 2.4
 *
 *  @package dokan
 */
?>
<div class="dokan-alert dokan-alert-warning">
    <!-- ###-tct_changed-### START-->
    <strong>Warning!</strong> <br/>
    <?php 
        $tct_admin_email=get_option( 'admin_email', 'info@thecooktown.com.au' );
        $tct_requirements_url=get_permalink(422);
    ?>
    Your account is not yet enabled for selling, make sure you have submitted all required documents to 
    <a href="mailto:<?php echo($tct_admin_email);?>"><?php echo($tct_admin_email); ?></a>.<br/>
    You can find a list of the requirements <a href="<?php echo($tct_requirements_url);?>" target="_blank">here</a>.
<!-- ###-tct_changed-### END-->

</div>
