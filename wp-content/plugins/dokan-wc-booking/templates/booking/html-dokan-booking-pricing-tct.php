<?php
$tct_percentage_commission = 100 - dokan_get_seller_percentage();
$booking_cost         = get_post_meta( $post_id, '_wc_booking_cost', true );
$booking_base_cost    = get_post_meta( $post_id, '_wc_booking_base_cost', true );
$booking_display_cost = get_post_meta( $post_id, '_wc_display_cost', true )
?>

<div class="cost_fields dokan-edit-row dokan-clearfix">
    <div class="dokan-section-heading" data-togglehandler="cost_fields">
        <h2><i class="fa fa-money" aria-hidden="true"></i> <?php _e( 'Price', 'dokan-wc-booking' ) ?></h2>
        <p><?php _e( 'Set the sales price', 'dokan-wc-booking' ) ?></p>
        <a href="#" class="dokan-section-toggle">
            <i class="fa fa-sort-desc fa-flip-vertical" aria-hidden="true" style="margin-top: 9px;"></i>
        </a>
        <div class="dokan-clearfix"></div>
    </div>
    <div id="bookings_pricing" class="dokan-section-content">
        <div class="dokan-form-group">
            <label for="_wc_booking_cost" class="form-label"><?php _e( 'Price per person', 'dokan-wc-booking' ); ?>
                <span class="dokan-tooltips-help tips" title="" data-original-title="Full price per menu per person (The Cook Town fees (<?php echo esc_attr($tct_percentage_commission); ?>%) will be discounted from this price)">
                    <i class="fa fa-question-circle"></i>
                </span>
            </label>
            <?php dokan_post_input_box( $post_id, '_wc_booking_cost', array( 'min' => '0', 'step' => '0.1', 'value' => $booking_cost ), 'number' ); 
                //dokan_post_input_box( $post_id, '_wc_booking_base_cost', array( 'min' => '0', 'step' => '0.1', 'value' => $booking_base_cost ), 'number' ); 
            ?>
        </div>
        <?php do_action( 'woocommerce_bookings_after_booking_base_cost', $post_id ); ?>

        <!-- hidden input to avoid "Notice" messages when saving -->
        <input type="hidden" name="_wc_booking_base_cost" value="0"/>
        <!-- <div class="dokan-form-group content-half-part"> Block cost 
            <label for="_wc_booking_base_cost" class="form-label"><?php _e( 'Block cost', 'dokan-wc-booking' ); ?>
                <span class="dokan-tooltips-help tips" title="" data-original-title="<?php _e( 'This is the cost per block booked. All other costs (for resources and persons) are added to this.', 'dokan-wc-booking' ); ?>">
                    <i class="fa fa-question-circle"></i>
                </span>
            </label>
            <?php //dokan_post_input_box( $post_id, '_wc_booking_base_cost', array( 'min' => '0', 'step' => '0.1', 'value' => $booking_base_cost ), 'number' ); ?>
        </div> -->
        <?php do_action( 'woocommerce_bookings_after_booking_block_cost', $post_id ); ?>
        
         <div style="display: none;" class="dokan-form-group content-half-part">
            <label for="_wc_display_cost" class="form-label"><?php _e( 'Display cost', 'dokan-wc-booking' ); ?>
                <span class="dokan-tooltips-help tips" title="" data-original-title="<?php _e( 'The cost is displayed to the user on the frontend. Leave blank to have it calculated for you. The Cook Town recomends to display the same base cost per Menu, so that customers can better filter by price.', 'dokan-wc-booking' ); ?>">
                    <i class="fa fa-question-circle"></i>
                </span>
            </label>
            <?php dokan_post_input_box( $post_id, '_wc_display_cost', array( 'min' => '0', 'step' => '0.1', 'value' => $booking_display_cost ), 'number' ); ?>
        </div>
        <?php do_action( 'woocommerce_bookings_after_display_cost', $post_id ); ?>

        

    </div>
</div>