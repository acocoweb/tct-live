<?php

//Remove New Post Types from The Gem Theme


function delete_post_type(){
    // Remove News
    unregister_post_type( 'thegem_news' );
    // Remove Clients
    unregister_post_type( 'thegem_client' );
    // Remove Portfolios
    unregister_post_type( 'thegem_pf_item' );
    // Remove Galleries
    unregister_post_type( 'thegem_gallery' );
    // Remove Teams
    unregister_post_type( 'thegem_team_person' );
    // Remove NivoSlider
    unregister_post_type( 'thegem_slide' );
    
}
add_action('init','delete_post_type');

// Cook Registration Wizard
require('tct_functions/cook_registration_wizard.php');

//changes to TheGem's copy of Woocomerce Product Page
require('tct_functions/tct_product_page_custom.php');

//changes to Dokan's  registration/login Page
require('tct_functions/tct_registration_login_page_custom.php');

//changes to Dokan's "Vendor" (Cook) profile page
require('tct_functions/tct_cook_store-form.php');

//changes to Dokan's "Vendor" (Cook) Dashboard
require('tct_functions/tct_dokan_dashboard_custom.php');

//changes to Dokan's "Store" (Cook) Page
require('tct_functions/tct_cook_store-page.php');

//require default values for post-meta for Menus when creating or editing Menus in Dokan Dashboard
require('tct_functions/tct_menu_post-meta_defaults.php');

//changes to simplify "new product" ("new menu") creation page
require('tct_functions/tct_dokan_new_menu_custom_page.php');

//changes to Dokan's "Store" list to make it a "Cooks" List
//include the new tct_cook_lists shortcode:
require('tct_shortcodes/tct_cook_lists_shortcode.php');

//changes to the Product-Search Results Items (loop)
require('tct_functions/tct_product_loop_custom.php');

//changes to the "My Account" page
require('tct_functions/tct_my_account.php');



// Enqueue WooCommerce, Dokan & TheGem customized css

function tct_enqueue_extra_styles() {

   //include modifications to thegem woocommerce customized css
    //wp_enqueue_style( 'tct-woocommerce-custom', get_template_directory_uri() . '-child/css/tct-woocommerce-custom.css', false, '0.1','all');
    $thegem_woo_style = 'thegem-woocommerce-custom'; // copied from thegem\inc\woocommerce.php
    //wp_enqueue_style( $parent_style, get_template_directory_uri() . '/css/thegem-woocommerce-custom.css' );
    wp_enqueue_style( 'tct-woocommerce-custom',
        get_stylesheet_directory_uri() . '/css/tct-woocommerce-custom.css',
        array( $thegem_woo_style ), '1.0'
        );

//include modification to dokan css
    $dokan_style = 'dokan-style'; // copied from plugins/dokan-lite/dokan.php
    wp_enqueue_style( 'tct-dokan-lite-custom',
        get_stylesheet_directory_uri() . '/css/tct-dokan-custom.css',
        array( $dokan_style ), '1.0'
        );
        
//include modifications to thegem-woocommerce (general) css

    $thegem_woo_style = 'thegem-woocommerce'; // copied from thegem\inc\woocommerce.php
    wp_enqueue_style( 'tct-woocommerce',
        get_stylesheet_directory_uri() . '/css/tct-woocommerce.css',
        array( $thegem_woo_style ), '1.0'
        );
    
//include modification to thegem-styles (general) css
    //$thegem_basic_style =""

//include additional custom The Cook Town Styles
    $thegem_new_style = 'thegem-new-css'; //copied from \themes\thegem\functions.php line 272 
    wp_enqueue_style ('tct-custom-styles',
        get_stylesheet_directory_uri() . '/css/tct-custom-styles.css',
        array( $thegem_new_style ), '1.0'
        );

//include modifications to woocommerce-bookings (frontend) css

    $woo_booking_frontend_style = 'wc-bookings-styles'; // copied from \woocommerce-bookings\woocommerce-bookings.php
    wp_enqueue_style( 'tct-woocommerce-bookings-custom',
        get_stylesheet_directory_uri() . '/css/tct-woocommerce-bookings-custom.css',
        array( $woo_booking_frontend_style ), '1.0'
        );


   
}
add_action( 'wp_enqueue_scripts', 'tct_enqueue_extra_styles' );


// Load translation files from child theme instead of the parent theme 
function tct_child_theme_locale() {
    load_child_theme_textdomain( 'total', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'tct_child_theme_locale' );


//Remove the "WP-Admin-Bar additional menus from The Gem Theme (useless and too big)

function tct_clean_wp_bar_menu () {

    remove_action('admin_bar_menu', 'thegem_admin_bar_site_menu', 100);
    remove_action('admin_bar_menu', 'thegem_admin_bar_thumbnails_generator', 101);
}

add_action('wp_loaded','tct_clean_wp_bar_menu');
?>