<?php
	$post_image = '';
	$attachment_id = get_post_thumbnail_id($post->ID);
	if ($attachment_id) {
		$post_image = thegem_generate_thumbnail_src($attachment_id, 'thegem-blog-timeline-large');
		if ($post_image && $post_image[0]) {
			$post_image = $post_image[0];
		}
	}
?>

<div class="socials-sharing socials socials-colored-hover tct-socials-sharing">
	<a class="socials-item" target="_blank" href="<?php echo esc_url('https://www.facebook.com/sharer/sharer.php?u='.urlencode(get_permalink())); ?>" title="Facebook"><i class="socials-item-icon facebook"></i></a>
	<a class="socials-item" target="_blank" href="<?php echo esc_url('https://twitter.com/intent/tweet?text='.urlencode(get_the_title()).'&amp;url='.urlencode(get_permalink())); ?>" title="Twitter"><i class="socials-item-icon twitter"></i></a>
	<a class="socials-item" target="_blank" href="<?php echo esc_url('https://plus.google.com/share?url='.urlencode(get_permalink())); ?>" title="Google Plus"><i class="socials-item-icon google-plus"></i></a>
	<a class="socials-item" target="_blank" href="<?php echo esc_url('https://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . ($post_image ? '&amp;media=' . urlencode($post_image) : '' )); ?>" title="Pinterest"><i class="socials-item-icon pinterest"></i></a>
</div>
