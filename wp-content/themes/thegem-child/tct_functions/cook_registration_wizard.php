<?php 
// Modifications to the Vendor (aka. "Cook") Registration Wizard
// Hooks from the "seller-setup-wizard.php" file

// First step in the registration "Welcome"
function  tct_cook_setup_introduction() {
    ?>
    <script>
        //Get the HTML of the name of the first step
        $step2Chng = document.querySelectorAll(".wc-setup-steps li")[0];
        //Change the Step name (before: "Store")
        $step2Chng.innerHTML = 'Cook Profile';

        //Get the HTML of the form
        $toChange = document.querySelectorAll(".wc-setup-content")[0];
        //Change the <h1>  (before: "Welcome to the Marketplace")
        $toChange.children[0].innerHTML ='Welcome to the Cook Town!';
        //Change the first <p> (before: "Thank you for choosing the Marketplace for...")
        $toChange.children[1].innerHTML = "Thank you for choosing The Cook Town to offer and promote your services as a cook! This quick setup wizard will help you configure the basic settings. <strong>It’s completely optional and shouldn’t take longer than two minutes.</strong>";
    </script>
    <?php
}
// Hook for the first step:
add_action( 'dokan_seller_wizard_introduction', 'tct_cook_setup_introduction' );

//Second Step "Store Setup"
function  tct_setup_cook_page() {
    ?>
    <script>
        //Get the HTML of the name of the first step
        $step2Chng = document.querySelectorAll(".wc-setup-steps li")[0];
        //Change the Step name (before: "Store")
        $step2Chng.innerHTML= "Cook Profile";

        // Get all elements in the form (class: "wc-setup-content")
        $toChange = document.querySelectorAll(".wc-setup-content")[0];
        
        //Change the Title (before: "Store Setup")
        $toChange.children[0].innerHTML = "Setup your Cook Profile";

        //The form is a table, get all rows from the form table
        $formToChange = document.querySelectorAll(".dokan-seller-setup-form .form-table tbody tr")
        
        //Change the first label of the form (before: "Store Product per Page")
        //The structure is: row.cell.label
        $formToChange[0].children[0].children[0].innerHTML = "Number of menus per page";
        

        //Reduce options in the Country selector
        // Row:5 , Cell:1 (data)
        
        //Australia Only:
        //$formToChange[5].children[1].children[0].innerHTML = '<option value="AU">Australia</option>';

        //Australia & New Zealand:
        $formToChange[5].children[1].children[0].innerHTML = '<option value="">- Select a Country -</option><option value="AU">Australia</option><option value="NZ">New Zealand</option>';

        // The rest of the labels remain the same (Street, City, etc.)
    </script>
<?php
}
//Hook for the second step:
//add_action( 'dokan_seller_wizard_after_store_setup_form', 'tct_setup_cook_page' );
add_action( 'dokan_seller_wizard_store_setup_field', 'tct_setup_cook_page' );

//Third step (Payment)
function tct_cook_payment_setup(){
    ?>
    <script>
         //Get the HTML of the name of the first step
         $step2Chng = document.querySelectorAll(".wc-setup-steps li")[0];
         //Change the Step name (before: "Store")
         $step2Chng.innerHTML = 'Cook Profile';
    </script>
    <?php

}

//Hook for the third step:
add_action( 'dokan_seller_wizard_after_payment_setup_form', 'tct_cook_payment_setup' );

