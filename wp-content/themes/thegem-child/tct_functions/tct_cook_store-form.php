<?php
// Add "description" field to the Cook Profile Page ("store-form" > Settings)

//Add the input field "Description" to the profile page of the cook
function tct_show_cook_profile_description ($current_user, $profile_info) {
    
    //if (!empty(get_user_meta($current_user, 'description', true))){
        $description = get_user_meta($current_user, 'description', true);
    //}
  
    ?>
    <div class="dokan-form-group">
        <label class="dokan-w3 dokan-control-label" for="setting_description"><?php echo 'Cook Profile'; ?></label>
        <div class="dokan-w5 dokan-text-left">
            <textarea id="setting_description" name="setting_description" placeholder="Specialties, Experience, etc..." rows="4" class="dokan-form-control input-md" form="store-form"><?php echo $description; ?></textarea>
        </div>
    </div>
    <?php
}

//Description will be added after the banner of the cook, before the profile picture and other data fields
add_action ('dokan_settings_after_banner', 'tct_show_cook_profile_description', 10, 2);


//Save the "Cook Profile" into the user's description field
function tct_save_cook_profile ($store_id) {

    if(isset( $_POST['setting_description'])) {
        $description = sanitize_text_field( $_POST['setting_description'] );
        //isset($_POST['tct_description']) ? $description=sanitize_text_field($_POST['tct_description']) : $description="";
        update_user_meta( $store_id, 'description', $description );
    }
}
//Save the description after all other fields are saved
add_action ('dokan_store_profile_saved', 'tct_save_cook_profile',10,1);


// Limit the Country List to AU and NZ (using Javascript)
function tct_change_cook_settings_form () {
    ?>
    <script>
        // select the country dropdown element class: .country_to_state
        $toChange = document.querySelectorAll(".country_to_state");

        //Reduce options in the Country selector
                
        //Australia Only:
        //$toChange[0].innerHTML= '<option value="AU">Australia</option>';

        //Australia & New Zealand:
        //$toChange[0].innerHTML='<option value="">- Select a Country -</option><optgroup label="------------------------------"><option value="AU">Australia</option><option value="NZ">New Zealand</option>';

    </script>
    <?php
}
add_action('dokan_settings_form_bottom','tct_change_cook_settings_form');
//remove, <?php do_action( 'dokan_settings_form_bottom', $current_user, $profile_info ); ?>
