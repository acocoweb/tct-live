<?php
//Modify the "Cook" Store page

// Change the name of the "Products" tab to "Menus"
function tct_change_cook_page_tabs ($tabs, $store_id ) {
    
    $cooks_tabs = $tabs;
    $cooks_tabs['products']['title'] = 'Menus';

    return $cooks_tabs;
}

add_filter( 'dokan_store_tabs', 'tct_change_cook_page_tabs', 20, 2 );

// Hide the Phone Number and E-Mail from Cook
// Hook: dokan_after_store_tabs (from: ..\plugins\dokan-lite\templates\store-header.php)
function tct_hide_cook_contact() {
    ?>
    <script>
        document.querySelectorAll("ul > li.dokan-store-address")[0].style.display="none";
        document.querySelectorAll("ul > li.dokan-store-phone")[0].style.display="none";
        document.querySelectorAll("ul > li.dokan-store-email")[0].style.display="none";
    </script>
<?php
}
add_action( 'dokan_after_store_tabs', 'tct_hide_cook_contact' ); 


// add <div> at the end of the Reviews template to separate the footer (correction to DOKAN-PRO)

function tct_separate_reviews_from_footer () {
    get_template_part('tct_templates/footer-separator');
}

add_action( 'woocommerce_after_main_content', 'tct_separate_reviews_from_footer' );


?>
