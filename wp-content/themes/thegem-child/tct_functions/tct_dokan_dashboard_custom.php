<?php 

/** Adjusts the array of the dashboard left-bar-menu for cooks */
/** Filter hook from  \dokan-lite\includes\template-tags.php */
/** Bookings item from  \dokan-wc-booking\dokan-wc-booking.php */
function tct_get_cooks_dashboard_nav($urls ) {

    $cooks_urls = $urls;
    
    // "Products" and "orders" menus are not needed by the cooks
    unset ($cooks_urls['products']);
    //unset ($cooks_urls['orders']); // orders must be visible, otherwise Cooks can't access the Delivery Addresses!
    // "Settings > Shipping" is also not needed by the cooks
    unset ($cooks_urls['settings']['sub']['shipping']);
    
    //"Bookings" is the most important menu point, so it should go higher
    // 'pos' was 180 before --> changed to 30 (the 'pos' of the deleted products menu)
    $cooks_urls['booking']['pos'] = 30;
    $cooks_urls['booking']['title'] = 'Menus & Bookings';
    
    return $cooks_urls;
}

add_filter( 'dokan_get_dashboard_nav', 'tct_get_cooks_dashboard_nav', 12, 1 );


//Change the Tab-Menus within the Bookings module
function tct_menus_n_bookings_tab_menu ($bookings){

    $tct_bookings=$bookings;

    $tct_bookings['']['title'] = 'All Menues';
    $tct_bookings['resources']['title'] = 'Cook Availability';

    return $tct_bookings;
}

add_filter( 'dokan_booking_nav_titles', 'tct_menus_n_bookings_tab_menu' );


//Change the text of "Add new menu" button (Before: "Add new booking product")
function tct_new_menu_btn(){
    
    ?>
    <script>
    
        $tct_new_menu_btn_toChange = document.querySelector(".dokan-add-product-link > a");
        if ($tct_new_menu_btn_toChange){
            $tct_new_menu_btn_toChange.innerHTML="<i class='fa fa-briefcase'>&nbsp;</i> Add New Menu";
        }
        
    </script>
    <?php 
}
add_action ('dokan_dashboard_content_inside_after','tct_new_menu_btn');

//Change the text of the "Add new Cook" Button (before: "Add new Resource")
function tct_new_cook_btn(){
    ?>
    <script>
        
        $tct_new_cook_btn_toChange = document.querySelector(".dokan-add-resource-link > button");
        if ($tct_new_cook_btn_toChange){
            $tct_new_cook_btn_toChange.innerHTML="<i class='fa fa-briefcase'>&nbsp;</i> Add New Cook";
        }
    </script>
    <?php

}
    
add_action ('dokan_dashboard_content_inside_before','tct_new_cook_btn');

//Change the title to "Add New Menu" in the page to add a new menu
function tct_add_new_menu_custom_page(){
    ?>
    <script>
        // $toChange = document.querySelector(".dokan-product-edit > header");
        // $toChange.innerHTML="<h1 class='entry-title'>Add New Menu</h1>";
        $toChange= document.querySelector(".dokan-product-edit > header > h1");
        if ($toChange.innerHTML.trim()=='Add Bookable product') {$toChange.innerHTML='Add New Menu';}
        
    </script>
    <?php

}

add_action ('dokan_dashboard_content_after','tct_add_new_menu_custom_page');


function tct_add_menu_help($dashboard_links){

    $dashboard_links = '<li class="dokan-common-links dokan-clearfix">
    <a title="' . __( 'Visit Store', 'dokan-lite' ) . '" class="tips" data-placement="top" href="' . dokan_get_store_url( dokan_get_current_user_id() ) .'" target="_blank"><i class="fa fa-external-link"></i></a>
    <a title="' . __( 'Help', 'dokan-lite' ) . '" class="tips" data-placement="top" href="' . get_permalink( get_page_by_title( 'Manual for Cooks' )) .'" target="_blank"><i class="fa fa-question-circle"></i></a>
    <a title="' . __( 'Edit Account', 'dokan-lite' ) . '" class="tips" data-placement="top" href="' . dokan_get_navigation_url( 'edit-account' ) . '"><i class="fa fa-user"></i></a>
    <a title="' . __( 'Log out', 'dokan-lite' ) . '" class="tips" data-placement="top" href="' . wp_logout_url( home_url() ) . '"><i class="fa fa-power-off"></i></a>
    </li>';

    return $dashboard_links;
/*<a title="' . __( 'Help', 'dokan-lite' ) . '" class="tips" data-placement="top" href="' . get_permalink( get_page_by_title( 'Manual for Cooks' ) .'" target="_blank"><i class="fa fa-question"></i></a>*/
}

add_filter ('dokan_dashboard_nav_common_link', 'tct_add_menu_help');


//Hide table showing base & block costs for resource when they've just been added (cooks should not have extra costs)
//Hook from WC-Bookings: ..\plugins\woocommerce-bookings\includes\admin\views\html-booking-resource.php
function tct_hide_resource_extra_costs(){
    ?>
    <script>
        document.querySelectorAll("table.wc-metabox-content")[0].style.display = 'none';
    </script>
    <?php
}

add_action ('woocommerce_bookings_after_resource_block_cost', 'tct_hide_resource_extra_costs');

?>