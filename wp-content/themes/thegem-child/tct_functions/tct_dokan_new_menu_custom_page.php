<?php
// Simplification of the Dokan "new-product" page for initial menu creation 
// Few fields are made available already in \wp-content\plugins\dokan-wc-booking\templates\booking\new-product-tct.php
// Here only the Categories Dropdown is reduced to 
// "menu" only
// so that only menus are created by the cooks
// 

//Action Hook: dokan_new_product_form from \plugins\dokan-lite\templates\products\new-product-tct.php

function tct_only_menu_cat () {

    ?>
    <script>
        document.getElementById("product_cat").innerHTML='<option class="level-0" value="41">Menu</option> <option class="level-0" value="139">Cooking Lessons</option> ';
    </script>
    <?php
}

// add_action('dokan_new_product_form','tct_only_menu_cat', 99);

?>