<?php

// Set the default "post-meta" values required for the menus to work according to The Cook Town's Terms & Conditions
// When the product is being updated 
// action hook: do_action( 'dokan_product_updated', $post_id );  from \plugins\dokan-lite\classes\template-products.php

function tct_set_menu_default_meta_updating ($post_id) {

    $tct_menu_defaults = array(
        '_wc_booking_apply_adjacent_buffer'	=>	 ''	,
        //'_wc_booking_buffer_period'	=>	 '1'	,
        '_wc_booking_calendar_display_mode'	=>	 'always_visible'	,
        '_wc_booking_cancel_limit_unit'	=>	 'minute'	,
        '_wc_booking_cancel_limit'	=>	 '1'	,
        '_wc_booking_check_availability_against'	=>	 ''	,
        '_wc_booking_duration_type'	=>	 'customer'	,
        '_wc_booking_duration_unit'	=>	 'hour'	,
        '_wc_booking_duration'	=>	 '1'	,
        '_wc_booking_enable_range_picker'	=>	 ''	,
        '_wc_booking_first_block_time'	=>	 '08:00'	,
        //'_wc_booking_has_person_types'	=>	 ''	,
        '_wc_booking_has_persons'	=>	 '1'	,
        '_wc_booking_has_resources'	=>	 '1'	,
        '_wc_booking_max_date_unit'	=>	 'month'	,
        '_wc_booking_max_date'	=>	 '6'	,
        '_wc_booking_min_date_unit'	=>	 'hour'	,
        '_wc_booking_min_date'	=>	 '16'	,
        '_wc_booking_max_duration'	=>	 '4'	, 
        '_wc_booking_min_duration'	=>	 '4'	,
        '_wc_booking_person_cost_multiplier'	=>	 '1'	,
        '_wc_booking_requires_confirmation'	=>	 ''	,
        '_wc_booking_resources_assignment'	=>	 'automatic'	,
        '_wc_booking_user_can_cancel'	=>	 '1'	,
        'wc_booking_resource_label'	=>	 'Cook'	,
        '_disable_shipping'	=>	 'no'	,
        '_overwrite_shipping'	=>	 'no'	,
        '_wc_booking_qty'       => '1'
       
    );

    foreach ($tct_menu_defaults as $meta_key => $def_value) {
        update_post_meta( $post_id, $meta_key, $def_value);
    }    
}

add_action ('dokan_product_updated','tct_set_menu_default_meta_updating', 30, 1);

// Set "Display Cost" to be the so called "Base cost" (Meta Key: "_wc_booking_cost")
// So that the cook doesn't have to enter it twice AND the customer always sees the price per person
function tct_set_display_price ($post_id){
    // Get the costs
    $tct_cost_meta = get_metadata('post', $post_id, '_wc_booking_cost');

    $tct_cost = $tct_cost_meta[0];

    update_post_meta( $post_id, '_wc_display_cost', $tct_cost);
    //die("tct_cost: ".$tct_cost. " || tct_base_cost_meta:". $tct_cost_meta)
    
}

add_action ('dokan_product_updated','tct_set_display_price', 45, 1);