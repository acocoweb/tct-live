<?php
/*
If the logged user is a COOK ("Seller") add the "Cook Dashboard" button
Else, change the Dokan link "Become a Vendor" to "Become a Cook"
*/
    function tct_no_cook_dashboard_4_customer() {
        $user = wp_get_current_user();
        if ( in_array( 'seller', (array) $user->roles ) ) {
            //get_template_part( 'tct_templates/tct_cook_dashboard_account_btn');
        }
        else {
            ?><script> 
            // Search for the "Become a Vendor" link, which should be the only one with p > a.btn.btn-primary
            document.querySelectorAll("p > a.btn.btn-primary")[0].innerHTML="Become a Cook";
            </script> <?php
            get_template_part ('tct_templates/tct_customer_my_account_btns');
        }
    }

    add_action( 'woocommerce_after_my_account', 'tct_no_cook_dashboard_4_customer', 99 );

/* Change the text in the "Go to Vendor Dashboard" button to say "Cook" */
    add_filter ('dokan_set_go_to_vendor_dashboard_btn_text', 'tct_go2cook_dashboard_text', 10, 1);
    function tct_go2cook_dashboard_text ($btn_text) {
        return __( 'Go to Cook Dashboard', 'dokan-lite' );

    }
?>
