<?php
// Changes to the Product Search Results
// Hook "woocommerce_after_shop_loop_item_title" 
// from: ..\themes\thegem\woocommerce\content-product.php

function tct_addMinPersons_menuItemBox () {
    // '_wc_booking_min_persons_group'
    $tct_menu_id= get_the_ID();

    $tct_has_persons = get_post_meta($tct_menu_id, '_wc_booking_has_persons', TRUE);

    if ($tct_has_persons == '1') {
        $tct_min_group = get_post_meta($tct_menu_id,'_wc_booking_min_persons_group', TRUE);
        if ($tct_min_group > '1') {
            ?>
            <div class="tct_min_pers">(min. <?php echo $tct_min_group ?> people)</div>
            <?php
        }
    } 
}

add_action ('woocommerce_after_shop_loop_item_title', 'tct_addMinPersons_menuItemBox', 15);

//Remove "add to cart" and "product link" icons from below the title of each product-item-box in the search results page
function tct_no_add2cart_in_loop (){
    remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
    remove_action('woocommerce_after_shop_loop_item', 'thegem_woocommerce_after_shop_loop_item_link', 15);
}

add_action('wp_loaded','tct_no_add2cart_in_loop');
?>