<?php
// //add Tabs to the left column --> 26.11.2018: Tabs moved back to the bottom to keep consistency with mobile  
// //add_action('thegem_woocommerce_single_product_left', 'woocommerce_output_product_data_tabs', 7);
// //add full product description to the right column
// add_action('thegem_woocommerce_single_product_right', 'thegem_woocommerce_single_product_page_content', 40);

//remove the tabs from the bottom --> 26.11.2018: Tabs moved back to the bottom to keep consistency with mobile 
 function tct_child_remove_data_tabs_from_prod_bottom() {
    remove_action('thegem_woocommerce_single_product_bottom', 'woocommerce_output_product_data_tabs', 5);
} 

// //remove the full product description from the bottom
//  function tct_child_remove_product_page_from_prod_bottom() {
//     remove_action('thegem_woocommerce_single_product_bottom', 'thegem_woocommerce_single_product_page_content', 15);
// } 

//remove "back to shop button" 
function tct_remove_back_2_shop_arrow_prod_page() {
    remove_action('thegem_woocommerce_single_product_right', 'thegem_woocommerce_back_to_shop_button', 5);
}

//remove "PREV" & "NEXT" buttons
function tct_remove_prev_next_prod_page() {
    remove_action('thegem_woocommerce_single_product_bottom', 'thegem_woocommerce_single_product_navigation', 10);
}

 //add_action('wp_loaded','tct_child_remove_data_tabs_from_prod_bottom');
 //--> 26.11.2018: Tabs moved back to the bottom to keep consistency with mobile 
//  add_action('wp_loaded','tct_child_remove_product_page_from_prod_bottom');
// --> 03.07.2019: Description moved back to the bottom
 add_action('wp_loaded','tct_remove_back_2_shop_arrow_prod_page');
 add_action('wp_loaded','tct_remove_prev_next_prod_page');
 



/* Formating Changes to the single product page */
add_action('thegem_woocommerce_single_product_bottom', 'tct_formating_changes_2_product_page' );
function tct_formating_changes_2_product_page () {
    ?>
    <script>
        //Change <label> to <h3> for the "time" label in the booking date/time picker
        document.querySelectorAll("form > DIV > DIV > label")[0].innerHTML="<h3><label for='wc_bookings_field_start_date'>Time:</label></h3>";

        //Move Product Name, Price & description to be above review/cook/menus tabs for MOBILE
        /* not needed if tabs are at the bottom --> 26.11.2018: Tabs moved back to the bottom to keep consistency with mobile  */
       /*if (screen.width <= 600)
        {
            $mainProductInfo = document.querySelectorAll("div.single-product-content-right")[0];
            $tabsLeft = document.querySelectorAll ("div.vc_tta-container")[0];
            $mainProductInfo.append($tabsLeft);
        }*/
    </script>
    <?php
}




//Change "Vendor" for "Cook" and "Product" for "Menu"  in the product tabs
add_filter( 'woocommerce_product_tabs', 'tct_cook_product_tab',98);
function tct_cook_product_tab( $tabs) {
    
        $tabs['seller']['title'] = 'Cook';
        //change the callback so that the body of the tab is TCT specific
        $tabs['seller']['callback'] = 'tct_cook_body_product_tab';
        $tabs['more_seller_product']['title'] = 'More Menus';

        // if (in_array('additional_information', $tabs)) {
        //     $tabs['additional_information'] = 'Menu Info';
        // }

        // Do not show "Shipping" tab in Product page
        if (array_key_exists('shipping', $tabs)) {
            unset($tabs['shipping']);
        }

        // Rename "Additional Information" tab, make it shorter: "Info"
        if (array_key_exists('additional_information', $tabs)) {
            $tabs['additional_information']['title'] = 'Info';
        }
                 
        
        return $tabs;
    }
    



// TCT Specific "Cook" Tab body
function tct_cook_body_product_tab( $val ) {
    global $product;

    $author_id  = get_post_field( 'post_author', $product->get_id() );
    $author     = get_user_by( 'id', $author_id );
    $store_info = dokan_get_store_info( $author->ID );

    ?>
    
    <h2><?php echo 'Cook Information'; ?></h2>
    
    <!-- Cook profile Picture + Link to Cook's store -->
    <div class="cook-avatar">
        <?php printf( '<a href="%s">%s</a>', dokan_get_store_url( $author->ID ), get_avatar( $author->ID, 150 ) );?>
    </div>

    <div class="cook-product-tab">
        <!-- Cook Store Name & Link -->
        <?php if ( !empty( $store_info['store_name'] ) ) { ?>
            <p class="store-name">
                <span><?php echo 'Menus from: '; ?></span>
                <span class="details">
                    <?php echo esc_html( $store_info['store_name'] ); ?>
                </span>
                <br/>
                <span class="seller-name"><?php echo 'Cook: '; ?></span>
                <span class="details">
                <?php printf( '<a href="%s">%s</a>', dokan_get_store_url( $author->ID ), $author->display_name ); ?>
                </span>
            </p>
        <?php } ?>
        
        <!-- Cook Rating -->
        <p class="clearfix">
            <?php dokan_get_readable_seller_rating( $author->ID ); ?>
        </p>
    </div>

    
<?php    
}

//Change formating of "Book now" button and make it more visible (using gem styles)
add_action('woocommerce_after_add_to_cart_button','tct_change_book_now_button');
function tct_change_book_now_button () {
    ?>
    <script>
    //get the "book now" button
    $toChange = document.querySelectorAll(".wc-bookings-booking-form-button.single_add_to_cart_button");
    
    //classes from "booking" button: wc-bookings-booking-form-button single_add_to_cart_button button alt disabled 
    //classes from "Gem" buttons gem-button gem-button-size-medium gem-button-style-flat gem-button-text-weight-normal
    //both class groups included
    $toChange[0].className="wc-bookings-booking-form-button single_add_to_cart_button button alt disabled gem-button gem-button-size-medium gem-button-style-flat gem-button-text-weight-normal";
    </script>
    <?php
}





//hide the "Duration" field 
add_action ('woocommerce_after_add_to_cart_button','tct_hide_duration');
function tct_hide_duration () {
    ?>
    <script>

        document.querySelector ("p.wc_bookings_field_duration").style="display:none;";
    </script>
    <?php
}





add_action('thegem_woocommerce_single_product_bottom', 'tct_add_description_title', 7);
function tct_add_description_title () {
    ?> <h3> Full Menu Description </h3> <?php
}

?>