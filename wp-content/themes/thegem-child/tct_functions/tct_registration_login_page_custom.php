<?php

//Change the word "Vendor" to "Cook" in the Registration/Login Page
function tct_registration_vendor2cook (){
    ?>
    <script>
        //Get the Text of the radio button with value "seller"
        //$toChange = document.querySelectorAll(".user-role .radio>input[value=seller]")[0].parentNode;
        //$toChange.innerText = "I am a cook";
        $toChange = document.querySelectorAll(".user-role .radio>input[value=seller]")[0].parentNode;
        $toChange.innerHTML="<input type=\"radio\" name=\"role\" value=\"seller\"> I am a cook";
    </script>
    <?php
}

add_action('woocommerce_register_form_end','tct_registration_vendor2cook');

//Change the words "Shop Name" and "Shop URL" for "Nickname" and "Cook URL"  in the Registration/Login Page
function tct_change_shop2cook_nickname(){
    ?>
    <script>
    $name2Nick = document.querySelector(".show_if_seller label[for=company-name]");
    $name2Nick.innerHTML = "Nickname <span class='required'>*</span> <small>(If you don’t have a nickname adopt Yourname.Yoursurname)</small>";

    $shop2cook = document.querySelector(".show_if_seller label[for=seller-url]");
    $shop2cook.innerHTML = "Cook URL <span class='required'>*</span>";
    </script>
    <?php
}
add_action ('dokan_seller_registration_field_after', 'tct_change_shop2cook_nickname');
?>