<?php
/* Copied from ...\plugins\dokan-lite\includes\template-tags.php 
/* Replaced to add custom filters for TCT
*/
function tct_cooks_listing( $atts ) {
    /**
     * Filter return the number of store listing number per page.
     *
     * @since 2.2
     *
     * @param array
     */
    $attr = shortcode_atts( apply_filters( 'dokan_store_listing_per_page', array(
        'per_page' => 10,
        'search'   => 'yes',
        'per_row'  => 3,
        'featured'  => 'no'
    ) ), $atts );
    $paged   = max( 1, get_query_var( 'paged' ) );
    $limit   = $attr['per_page'];
    $offset  = ( $paged - 1 ) * $limit;

    $seller_args = array(
        'number' => $limit,
        'offset' => $offset
    );

    // if search is enabled, perform a search
    if ( 'yes' == $attr['search'] ) {
        $search_term = isset( $_GET['dokan_seller_search'] ) ? sanitize_text_field( $_GET['dokan_seller_search'] ) : '';
        if ( '' != $search_term ) {

            $seller_args['meta_query'] = array(
                array(
                    'key'     => 'dokan_enable_selling',
                    'value'   => 'yes',
                    'compare' => '='
                ),
                 array(
                    'key'     => 'dokan_store_name',
                    'value'   => $search_term,
                    'compare' => 'LIKE'
                )
            );
        }
    }

    if ( $attr['featured'] == 'yes' ) {
        $seller_args['meta_query'][] = array(
                                        'key'     => 'dokan_feature_seller',
                                        'value'   => 'yes',
                                        'compare' => '='
                                    );
    }

    $sellers = dokan_get_sellers( apply_filters( 'dokan_seller_listing_args', $seller_args ) );

    /**
     * Filter for store listing args
     *
     * @since 2.4.9
     */
    $template_args = apply_filters( 'dokan_store_list_args', array(
        'sellers'    => $sellers,
        'limit'      => $limit,
        'offset'     => $offset,
        'paged'      => $paged,
        'image_size' => 'full',
        'search'     => $attr['search'],
        'per_row'    => $attr['per_row']
    ) );
    ob_start();
    dokan_get_template_part( 'tct_cook_lists', false, $template_args );

    $content = ob_get_clean();

    return apply_filters( 'dokan_seller_listing', $content, $attr );
    
}

add_shortcode( 'tct-cooks-list', 'tct_cooks_listing' );
?>