<?php
/*
Button to go to Cook's Dashboard from My-account page
*/

    $dashboard_url = get_home_url().'/dashboard/';
?>

<p>&nbsp;</p>
<p style="text-align: right;">Are you a cook? Go to your Cook's dashboard <a href="<?php _e($dashboard_url);?>">here</a></br>
<a class="gem-button gem-button-size-small gem-button-style-flat gem-button-text-weight-normal" style="border-radius: 3px;" href="<?php  _e($dashboard_url); ?>" target="_self">Cook's Dashboard</a></p>