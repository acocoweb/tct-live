<?php
/*
Buttons for customers to go search for Menus or Cooks 
*/

    $tct_menus_url = get_home_url().'/product-category/menu/';
    $tct_cooks_url = get_home_url().'/cooks';
?>

<p>
    <a class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2" style="border-radius: 3px;" href="<?php _e($tct_menus_url); ?>" target="_self">Search for Menus</a>    
    <a class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2" style="border-radius: 3px;" href="<?php _e($tct_cooks_url); ?>" target="_self">Select a Cook</a>    
    
    <!-- brown: gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 -->
    <!-- orange: gem-button gem-button-size-small gem-button-style-flat gem-button-text-weight-normal -->
</p>