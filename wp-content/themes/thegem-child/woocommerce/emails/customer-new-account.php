<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s Customer first name */ ?>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>
<p><?php printf( __( 'Thanks for creating an account on %1$s. As a reminder, the username you chose is %2$s. You can access your account area to view orders, change your password, and more at: %3$s', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); ?></p><?php // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped ?>

<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
	<?php /* translators: %s Auto generated password */ ?>
	<p><?php printf( esc_html__( 'Your password has been automatically generated: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
<?php endif; ?>

<p><?php esc_html_e( 'We look forward to seeing you soon.', 'woocommerce' ); ?></p>

<?php

$tct_user = get_user_by('login', $user_login ); 

if ( in_array( 'seller', (array) $tct_user->roles ) ) {
	?>
	<p><strong> Important Information for Cooks:</strong></p>
	<p><?php printf('Please see the followings steps to complete the verification and become a Cook!');?></p>
	<ol>
		<li>
			<?php printf(
				'<strong style="color:#f39200;">Make sure you submit all documents required for cooks to:</strong> %s.', 
				make_clickable(get_option( 'admin_email', 'info@thecooktown.com.au' )));
				printf (
					'<br/>(You can find the list of all requirements under the following link: <br/>%s.', 
					make_clickable( esc_url( get_permalink(422) ) ));
				printf (')');
			?>
		</li>
		<li>
			<?php printf(
				'Complete the ID verification and Address verification at: <br/>%s ',
				make_clickable (esc_url( get_permalink(11).'settings/verification/')));
			?>
		</li>
	</ol>
	<p>Once these two steps are completed, you will be contacted within 24 hours by our team and you will be able to create you own menus and become a cook for “The Cook Town”</p>
	<p>If you have any queries please contact us at <?php printf(make_clickable(get_option( 'admin_email', 'info@thecooktown.com.au' )));?></p>
	<p>Regards,</p>
	<h3>The Cook Town</h3>

	<?php
}
?>
<p><small><i><strong>Email Disclaimer<br/></strong>
		This email and any attachments are proprietary and confidential and are intended solely for the use of the individual to whom it is addressed.<br/>
		If you have received this email in error, please let us know immediately by reply email and delete it from your system. You may not use, disseminate, distribute or copy this message nor disclose its contents to anyone.
		</i></small>
</p>
<?php

do_action( 'woocommerce_email_footer', $email );
?>
